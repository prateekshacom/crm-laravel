<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
});

Route::prefix('contacts')->group(function () {

    Route::get('/', '\App\Http\Controllers\Contacts@index')->name('contacts'); 
    Route::get('/view', '\App\Http\Controllers\Contacts@view')->name('contacts-view'); 
    Route::get('/add', '\App\Http\Controllers\Contacts@add')->name('contacts-add'); 
    Route::get('/edit', '\App\Http\Controllers\Contacts@edit')->name('contacts-edit'); 
    Route::post('/delete', '\App\Http\Controllers\Contacts@delete')->name('contacts-delete'); 

});

Route::prefix('projects')->group(function () {

    Route::get('/', '\App\Http\Controllers\Projects@index')->name('projects'); 
    Route::get('/view', '\App\Http\Controllers\Projects@view')->name('projects-view'); 
    Route::get('/add', '\App\Http\Controllers\Projects@add')->name('projects-add'); 
    Route::get('/edit', '\App\Http\Controllers\Projects@edit')->name('projects-edit'); 
    Route::post('/delete', '\App\Http\Controllers\Projects@delete')->name('projects-delete'); 

});

