<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Contacts extends Controller
{
    //

    public function index(Request $request, \App\Models\Contact $contacts)
    {
        $posts = $contacts->get();
           // ->orderBy($order, $order_dir)
            //->paginate($posts_per_page);
    

        return view('contacts.default')->with('posts', $posts);
    }

    public function add()
    {
        return view('contacts.edit');
    }

    public function edit()
    {
        echo 'nikhil-edit';
    }

    public function delete()
    {
        echo 'nikhil-delete';
    }

    public function view()
    {
        echo 'nikhil-view';
    }
}
