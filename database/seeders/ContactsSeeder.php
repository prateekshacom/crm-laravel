<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

class ContactsSeeder extends Seeder
{
    var $faker = NULL;

    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 1; $i <= 20; $i++) {
            DB::table('contacts')->insert([
                'title' => $this->faker->sentence(3),
                'content' => $this->faker->sentence(24),
            ]);
        }
    }
}
